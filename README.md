# IBCTrack

With this Project I intend to monitor the level of an 1000L IBC Container. I use a Wemos D1 mini and a Lorawan module.

## Getting started

Wire everything accordingly and flash the Code with Arduino IDE

## Hardware

This Project uses
- Wemos D1 mini (Microcontroller)
- HX711 (AD Converter)
- MPX53DP (Differential Pressure Sensor)
- Feather (LoRaWan Radio Module)